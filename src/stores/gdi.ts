import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

function drillDownIntoAllAndCopy (thisLevel) {

    // recurse into all children
    Object.keys(thisLevel)
        .filter(x => x !== 'ALL' && typeof(thisLevel[x]) !== 'number')
        .map(k => drillDownIntoAllAndCopy(thisLevel[k]));

    if (thisLevel.ALL) {
        let next = thisLevel;
        while (next.ALL) {
            next = next.ALL;
        }
        thisLevel = Object.assign(thisLevel, next);
        delete thisLevel.ALL;
    }

    return thisLevel;
}

// make the data look like the treemap example expects it:
// https://echarts.apache.org/examples/en/editor.html?c=treemap-disk
function shapeAsChildren (name, obj, metric) {
    return {
        name,
        value: obj[metric],

        children: Object.keys(obj)
            .filter(k => typeof(k) !== 'number')
            .map(k => shapeAsChildren(k, obj[k], metric)),

        metrics: Object.keys(obj)
            .filter(k => typeof(k) === 'number')
            .reduce((a, k) => a[k] = obj[k], {}),
    };
}

export const useGDIStore = defineStore('gdi', {
    state: () => {
        return {
            rawData: null,
            metric: 'affiliate_max_size',
        }
    },

    getters: {
        // TODO: executes before rawData is loaded, how do we delay it?
        metrics: (state) => {
            if (!state.rawData) { return []; }

            return Object.keys( state.rawData.ALL.ALL.ALL )
                .map( (metric) => ({ label: metric, value: metric }) )
        },

        dataForWorldMap: (state) => {
            // TODO: find out how to delay dataForWorldMap so this check is not necessary
            if (!state.rawData) { return {}; }

            const { metric, rawData } = state;

            var continents = Object.keys(rawData);

            return continents.reduce(function (accumulator, continent) {
                return accumulator.concat(Object.keys(rawData[continent]).reduce(function (countries, subcontinent) {
                    return countries.concat(Object.keys(rawData[continent][subcontinent]).map(function (country) {
                        return {
                            name: country,
                            value: rawData[continent][subcontinent][country][metric],
                            continent,
                            subcontinent,
                        };
                    }));
                }, []));
            }, []).filter(c => c.name !== 'ALL');
        },

        dataForTreeMap: (state) => {
            // TODO: find out how to delay dataForWorldMap so this check is not necessary
            if (!state.rawData) { return {}; }

            const { metric, rawData } = state;

            const clone = JSON.parse(JSON.stringify(rawData));
            const aggregatesMoved = drillDownIntoAllAndCopy(clone);

            const transformed = shapeAsChildren('World', aggregatesMoved, metric);

            return transformed.children;
        },
    },

    actions: {
        async load() {
            const res = await fetch('/stubs/equity_landscape_input_metrics.json');
            this.rawData = await res.json();
        },
    },
})
